# Unofficial Beamer template based on Inria color palette

[Example slides](demo/main.pdf)

Feel free to modify this template and submit MR if you have some improvements in mind.
